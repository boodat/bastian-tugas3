import * as React from 'react';
import { Button, View, Text, StyleSheet, ScrollView } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import {ProgressView} from "@react-native-community/progress-view";

export default function ViewProgress({ navigation }) {
    return (
        <ScrollView >
            <View style={styles.container}>
            <View style={styles.contentSpace}>
                <Text style={styles.boldText}>Progress View</Text>
            <ProgressView
          progressTintColor="red"
          trackTintColor="blue"
          progress={0.9}
/>
            </View>

            <View style={styles.contentSpace}>
            <Text style={styles.boldText}>Progress View 20%</Text>
            <ProgressView
          progressTintColor="green"
          trackTintColor="cyan"
          progress={0.2}
/>
            </View>

            <View style={styles.contentSpace}>
            <Text style={styles.boldText} >Progress View isIndeterminate</Text>
            <ProgressView
          progressTintColor="black"
          isIndeterminate={true}
          trackTintColor="cyan"
          progress={0.4}
/>
            </View>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'white',
      padding: 10,
      alignContent: 'space-between',
    //   alignItems: 'center',
    //   justifyContent: 'center',
    },
    boldText: {
      fontSize: 18,
      color: '#022c4a',
    //   marginVertical: 16,
    },
    contentSpace: {
        flex: 2,
        marginVertical: 20,
    }
  });