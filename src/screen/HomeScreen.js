import * as React from 'react';
import { Button, View, Text, StyleSheet, ScrollView } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

// const Stack = createNativeStackNavigator();

export default function Home({ navigation }) {
    return (
      <ScrollView>
      <View style={styles.container}>
        <View style={{paddingBottom: 10,}}>
        <Button
        title='NetInfo'
        onPress={() => navigation.navigate('GetNetInfo')}
        style={styles.tombols}>
        </Button>
        </View>

        <View style={{paddingBottom: 10,}}>
        <Button
        title='DateTimePicker'
        onPress={() => navigation.navigate('Picker')}
        style={styles.tombols}>
        </Button>
        </View>

        <View style={{paddingBottom: 10,}}>
        <Button
        title='Sliders'
        onPress={() => navigation.navigate('Sliders')}
        style={styles.tombols}>
        </Button>
        </View>

        <View style={{paddingBottom: 10,}}>
        <Button
        title='GeoLocation'
        onPress={() => navigation.navigate('Geoloc')}
        style={styles.tombols}>
        </Button>
        </View>
        
        <View style={{paddingBottom: 10,}}>
        <Button
        title='Progress View'
        onPress={() => navigation.navigate('ViewProgress')}
        style={styles.tombols}>
        </Button>
        </View>
        
        <View style={{paddingBottom: 10,}}>
        <Button
        title='Progress Bar Android'
        onPress={() => navigation.navigate('ProgBarAndroid')}
        style={styles.tombols}>
        </Button>
        </View>

        <View style={{paddingBottom: 10,}}>
        <Button
        title='Clipboard'
        onPress={() => navigation.navigate('PapanKlip')}
        style={styles.tombols}>
        </Button>
        </View>

        <View style={{paddingBottom: 10,}}>
        <Button
        title='Async Storage'
        // onPress={() => navigation.navigate('Geoloc')}
        style={styles.tombols}>
        </Button>
        </View>

        <View style={{paddingBottom: 10,}}>
        <Button
        title='Snap Carousel'
        onPress={() => navigation.navigate('CarouselPage')}
        style={styles.tombols}>
        </Button>
        </View>

        <View style={{paddingBottom: 10,}}>
        <Button
        title='Image Zoom Viewer'
        onPress={() => navigation.navigate('Perbesar')}
        style={styles.tombols}>
        </Button>
        </View>

        <View style={{paddingBottom: 10,}}>
        <Button
        title='Linear Gradient'
        onPress={() => navigation.navigate('GradientPage')}
        style={styles.tombols}>
        </Button>
        </View>

        <View style={{paddingBottom: 10,}}>
        <Button
        title='Render Html'
        onPress={() => navigation.navigate('RenderPage')}
        style={styles.tombols}>
        </Button>
        </View>

        <View style={{paddingBottom: 10,}}>
        <Button
        title='Share'
        onPress={() => navigation.navigate('SharePage')}
        style={styles.tombols}>
        </Button>
        </View>

        <View style={{paddingBottom: 10,}}>
        <Button
        title='Skeleton Place Holder'
        // onPress={() => navigation.navigate('Tengkorak')}
        style={styles.tombols}>
        </Button>
        </View>

        <View style={{paddingBottom: 10,}}>
        <Button
        title='Web View'
        // onPress={() => navigation.navigate('WebViewPage')}
        style={styles.tombols}>
        </Button>
        </View>

        <View style={{paddingBottom: 10,}}>
        <Button
        title='Tool Tip'
        onPress={() => navigation.navigate('ToolTipPage')}
        style={styles.tombols}>
        </Button>
        </View>

        <View style={{paddingBottom: 10,}}>
        <Button
        title='Vector Icons'
        onPress={() => navigation.navigate('IconPage')}
        style={styles.tombols}>
        </Button>
        </View>

      </View>
      </ScrollView>

      
    );
  }

  const styles= StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        // alignContent :'space-around',
        marginHorizontal: 30,
        
    },
    tombols: {
        borderRadius: 100,
        marginVertical: 30,
    }
  })