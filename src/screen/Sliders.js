import React, {useEffect, useState} from 'react';
import { Button, View, Text, StyleSheet, TextInput, Alert } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Slider from '@react-native-community/slider';


export default function Sliders({ navigation }) {

    const [nama, setNama] = useState("");
    const [tinggi, setTinggi] = useState(0);
    const [berat, setBerat] = useState(0);
    return (
    <View style={styles.container}>
      <View style={styles.rows}>
        <Text style={{
          textAlign: 'center',
          fontSize: 30,
          color: 'blue'}}>Slider ni bwangg</Text>
      </View>

      <View style={styles.rows}>
        <TextInput 
        placeholder='Nama Lengkap'
        onChangeText={(text) => setNama(text)}
        value={nama}></TextInput>
      </View>

      <View style={styles.rows}>
        <Text style={{fontSize: 20,}}>Tinggi: {Math.floor(tinggi)} Kg</Text>
      </View>

      <View style={styles.rows}>
        <Slider style = {{width: 200, height: 50,}}
        maximumValue={200}
        minimumValue={90}
        onValueChange={(value) => setTinggi((Math.floor(value)))}
        thumbTintColor='yellow'>

        </Slider>
      </View>

      <View style={styles.rows}>
        <Text style={{fontSize: 20,}}>Berat: {Math.floor(berat)} Cm</Text>
      </View>

      <View style={styles.rows}>
        <Slider style = {{width: 200, height: 50,}}
        maximumValue={120}
        minimumValue={4}
        onValueChange={(value) => setBerat(Math.floor(value))}
        thumbTintColor='blue'>

        </Slider>
      </View>

      <View style={styles.rows}>
        <Button
        title='Data Saya'
        onPress={() => Alert.alert("Halo tuan " + nama + ", Tinggi tuan adalah " + tinggi + " Cm, & Berat tuan adalah " + berat + " Kg." )}></Button>
      </View>
        
      </View>

      
    );
  }

  const styles= StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        // alignContent :'space-around',
        marginHorizontal: 30,
        
    },
    tombols: {
        borderRadius: 100,
        marginVertical: 30,
    },
    rows: {
      // alignItems: 'center',
      marginVertical: 10,
    }
  })