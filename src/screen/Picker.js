import React, {useEffect, useState} from 'react';
import { Button, View, Text, StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import DateTimePicker from '@react-native-community/datetimepicker';


export default function Picker({ navigation }) {
  const [date, setDate] = useState(new Date());
  const [show, setShow ]= useState(false);
  const [mode, setMode] = useState("date");

  const onChange = (e, selectedDate) => {
    setDate(selectedDate);
    setShow(false);
  };

  const showMode = (modeToShow) => {
    setShow(true);
    setMode(modeToShow);
  };
    return (
    <View style={styles.container}>
      <View style={styles.rows}>
        <Text style={{
          textAlign: 'center',
          fontSize: 25,
          color: 'blue'}}>{date.toLocaleString()}</Text>
      </View>
      
      <View style={styles.rows}>
        <Button 
        title='Tanggal'
        onPress={() => showMode("date")}
        style={styles.tombols}></Button>
      </View>
      <View style={styles.rows}>
        <Button 
        title='Jam'
        onPress={() => showMode("time")}
        style={styles.tombols}></Button>
      </View>
        {show && (
            <DateTimePicker
            value={date}
            mode={mode}
            is24Hour = {true}
            onChange={onChange}
            ></DateTimePicker>
          )
        }

        
      </View>

      
    );
  }

  const styles= StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        // alignContent :'space-around',
        marginHorizontal: 30,
        
    },
    tombols: {
        borderRadius: 100,
        marginVertical: 30,
    },
    rows: {
      // alignItems: 'center',
      marginVertical: 10,
    }
  })