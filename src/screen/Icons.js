import Icon from 'react-native-vector-icons/FontAwesome5';
import React, {useEffect, useState} from 'react';
import {View, StyleSheet, Text} from 'react-native';

export default function IconPage({ navigation }) {
    return (
      <View style={styles.container}>
        <Text>Ini Adalah Icon </Text>
        <Icon name="user" size={30} color="#900" />
        {/* Icon-nya kotak-kotak gak jelas. Lebih Jelas Kepala Adudu... */}
        </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },


})
