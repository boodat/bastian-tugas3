import React, {useEffect, useState} from 'react';
import NetInfo from '@react-native-community/netinfo';
import { Button, View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

// const Stack = createNativeStackNavigator();

export default function GetNetInfo ({ navigation }) {
  const [netInfo, setNetInfo] = useState('');

  useEffect(()=>{
    const data = NetInfo.addEventListener((state)=>{
      setNetInfo(`connectionType: ${state.type
      } isConnected?: ${state.isConnected}`)
    });

    return()=>{
      data()
    }
  },[])

  const handleGetNetInfo = () => {
    NetInfo.fetch().then((state)=>{
      console.log(state)
      alert(`connectionType: ${state.type
      } isConnected?: ${state.isConnected}`)
    })
  };
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>
            {netInfo}
        </Text>

        
        <Button
        title='Get Information'
        onPress={()=> {handleGetNetInfo()}}></Button>
      </View>

      
    );
  }