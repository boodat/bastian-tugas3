import LinearGradient from 'react-native-linear-gradient';
import React from 'react';
import {SafeAreaView, StyleSheet, View, Text} from 'react-native';



export default function GradientPage({ navigation }) {
    return (
    <View style={styles.container}>
        <View style={{
            marginTop: 10,
        }}>
        <LinearGradient colors={['#4c669f', '#3b5998', '#192f6a']} style={styles.linearGradient}>
            <Text style={styles.buttonText}>
                Sign in with Facebook
                </Text>
                </LinearGradient>
        </View>
        <View style={{
            marginTop: 10,
        }}>
            <LinearGradient
            start={{x: 0, y: 0.75}} end={{x: 1, y: 0.25}}
            locations={[0,0.2,0.4,0.2,0.8,0.8]}
            colors={['#ff0000', '#fff700', '#09ff00', '#09ff00', '#0400ff', '#ea00ff']}
            style={styles.linearGradient}>
                <Text style={styles.buttonText}>
                    Pelangi Hehe
                    </Text>
                </LinearGradient>
        </View>
    </View>



);
};

// Later on in your styles..
var styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      },
  linearGradient: {
    // flex: 0.28,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 5
  },
  buttonText: {
    fontSize: 18,
    fontFamily: 'Gill Sans',
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent',

  },
});