import React, {useState} from 'react';
import Clipboard from '@react-native-community/clipboard';
import {
  SafeAreaView,
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  TextInput,
  Button,
} from 'react-native';

export default function PapanKlip({ navigation }) {
  const [copiedText, setCopiedText] = useState('');

  const copyToClipboard = () => {
    Clipboard.setString(inputkata);
  };

  const [inputkata, setKata] = useState("");

  const fetchCopiedText = async () => {
    const text = await Clipboard.getString();
    setCopiedText(text);
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={styles.container}>

      <View style={styles.rows}>
        <TextInput style={styles.input}
        placeholder='Masukkan kata-kata mutiara'
        onChangeText={(text) => setKata(text)}
        value={inputkata}></TextInput>
      </View>

      <Button
        title='Copy ke clipboard'
        onPress={copyToClipboard}
        color="#fe4a49"/>
        
        <View style={{ marginTop: 20 }}>
        <Button
        title='Tampilkan teks clipboard'
        onPress={fetchCopiedText}
        color="#fe4a49"/>
        </View>
        

        <Text style={styles.copiedText}>{copiedText}</Text>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems : 'center',
    marginHorizontal: 10,
  },
  copiedText: {
    marginTop: 10,
    color: 'blue',
    fontSize: 20,

  },
  rows: {
    // alignItems: 'center',
    marginVertical: 10,
  },
  tombols: {
    borderRadius: 100,
    marginVertical: 30,
},
input: {
  borderColor: "gray",
  width: "100%",
  borderWidth: 1,
  borderRadius: 10,
  padding: 10,
},
});
