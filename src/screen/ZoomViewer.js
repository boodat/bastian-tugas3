import React from 'react';

import {SafeAreaView, StyleSheet, View} from 'react-native';

import ImageViewer from 'react-native-image-zoom-viewer';

export default function Perbesar({ navigation }) {
  const gambar = [
    {
      url:
        'https://cdn.pnghd.pics/data/332/gambar-gambar-monyet-lucu-20.jpg',
    },

  ];

  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={styles.container}>
        <ImageViewer
          imageUrls={gambar}
          renderIndicator={() => null}
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    // backgroundColor: 'white',
    flex: 1,
  },
});
