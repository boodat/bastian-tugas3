import React from 'react';
import { useWindowDimensions, ScrollView } from 'react-native';
import RenderHtml from 'react-native-render-html';

const source = {
    html: `
    <header style="max-width: 900px;
      overflow: hidden;
      box-sizing: border-box;
      margin: 10px auto;
      text-align: center;">
      <nav style="padding: 1px 0 0;
          max-width: 200px;
          margin: 0 auto;">

            <em>Ini Halaman HTML</em>
            <p>Yang Dirender, Keren kan!</p>
      </nav>
    </header>
    <header style="display: none;
      position: fixed;
      width:100%,
      top: 0;
      background: #FFFFFF;
      margin: 0 -15px;
      width: 100%;
      border-bottom: 1px solid #CCCCCC;
      box-sizing: border-box;
      box-shadow: 0px 0px 10px 0 rgba(0, 0, 0, 0.5);
      opacity: 0.9;
      z-index: 100;">
      <div style="max-width: 1200px;
          padding: 15px 30px;
          margin: 0 auto;
          overflow: hidden;
          box-sizing: border-box;">
          <nav style="padding: 5px 0;
            max-width: 400px;
            float: right;
            text-align: right;">
            <a style="display: inline-block;
                color: #666666;
                text-decoration: none;
                font-size: 14px;
                cursor: pointer;" href="#section1">Section 1</a>
            <a style="display: inline-block;
                color: #666666;
                text-decoration: none;
                font-size: 14px;
                cursor: pointer;" href="#section2">Section 2</a>
          </nav>
      </div>
    </header>
    <div style="min-width: 300px;
      margin: 0 auto;
      height: 1000px;
      position: relative;
    ">
      <div style="min-height: 100px;position: relative; background: #ffd6cd; width=100vw;" id="section1">
          <h1 style="text-align: center;
            line-height: 500px;
            color: #666666;
            margin: 0;">Sesi Pertama</h1>
      </div>
      <div style="min-height: 100px;position: relative; background: #ddebfd; width=100vw;" id="section2">
          <h1 style="text-align: center;
            line-height: 500px;
            color: #666666;
            margin: 0;">Sesi Ke Dua</h1>
      </div>
    </div>
    </div>
  `
};

export default function RenderPage({ navigation }) {
  const { width } = useWindowDimensions();
  return (
    <ScrollView>
    <RenderHtml
      contentWidth={width}
      source={source}
    /></ScrollView>
  );
}