// In App.js in a new project
import Home from './src/screen/HomeScreen';
import GetNetInfo from './src/screen/InfoNet';
import Picker from './src/screen/Picker';
import Sliders from './src/screen/Sliders';
import CarouselPage from './src/screen/CarouselPage';
import Geoloc from './src/screen/Geolocation';
import PapanKlip from './src/screen/ClipBoard';
import Perbesar from './src/screen/ZoomViewer';
import ViewProgress from './src/screen/ProgressView';
import ProgBarAndroid from './src/screen/ProgressBarAndroid';
import SharePage from './src/screen/Share';
import GradientPage from './src/screen/LinearGradient';
import RenderPage from './src/screen/RenderHtml';
import ToolTipPage from './src/screen/ToolTippage';
import IconPage from './src/screen/Icons';
import React, {useEffect, useState} from 'react';
import { View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import NetInfo from '@react-native-community/netinfo';
import DateTimePicker from '@react-native-community/datetimepicker';
import Geolocation from '@react-native-community/geolocation';
import ProgressView from './src/screen/ProgressView';



const Stack = createNativeStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="GetNetInfo" component={GetNetInfo} />
        <Stack.Screen name="Picker" component={Picker} />
        <Stack.Screen name="Sliders" component={Sliders} />
        <Stack.Screen name="Geoloc" component={Geoloc} />
        <Stack.Screen name="ViewProgress" component={ViewProgress} />
        <Stack.Screen name="PapanKlip" component={PapanKlip} />
        <Stack.Screen name="Perbesar" component={Perbesar} />
        <Stack.Screen name="ProgBarAndroid" component={ProgBarAndroid} />
        <Stack.Screen name="SharePage" component={SharePage} />
        <Stack.Screen name="GradientPage" component={GradientPage} />
        <Stack.Screen name="RenderPage" component={RenderPage} />
        <Stack.Screen name="ToolTipPage" component={ToolTipPage} />
        <Stack.Screen name="IconPage" component={IconPage} />
        <Stack.Screen name="CarouselPage" component={CarouselPage} />

        {/* Saran Disini->   .. */}
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;